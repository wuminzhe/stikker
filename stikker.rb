require "sha1"
require "fileutils"
require "uri"
require 'open-uri'
require 'pathname'

def get_content(str, clength)
  result = []
  lines = str.strip.split("\n")
  lines.each do |line|
    result << get_content_no_new_line(line, clength)
  end
  return result.join("\n")
end

def get_content_no_new_line(str, clength)
  str = str.strip
  result = ''
  length = 0
  chars = str.scan(/./mu)
  chars.each do |c|
    if c.bytes.count==1
      length = length + 0.5
    else
      length = length + 1.0
    end
    if length.ceil == clength and not result.end_with?("\n")
      result = result + c + "\n"
      length = 0
    else
      result = result + c
    end
  end

  return result.strip #如果正好是结尾有个换行，那么去掉
end

class Stikker
  def initialize(background_image)
    @dir = Pathname.new(File.dirname(__FILE__)).realpath
    #
    if background_image.start_with? 'http'
      background_image = download(background_image)
    else
      background_image = nil if not FileTest.exist?(background_image)
    end

    raise "背景图片必须设置" if background_image.nil?

    @command = ["convert '#{background_image}'"]
  end

  def add_text(x, y, content, config={})
    x = x.nil? ? 0 : x
    y = y.nil? ? 0 : y

    content = content.nil? ? '' : content.gsub('"', '\"').gsub('`', "'")
    
    #
    clength = config['clength'].nil?      ? '16'                      : config['clength']
    #size =  config['size'].nil?           ? '560x'                    : config['size']
    font =  config['font'].nil?           ? "#{@dir}/fonts/kaiti.ttf" : config['font']
    fontsize = config['fontsize'].nil?    ? '21'                      : config['fontsize']
    fontcolor = config['fontcolor'].nil?  ? '#000000'                 : config['fontcolor']
    kerning = config['kerning'].nil?      ? '0'                       : config['kerning']
    bgcolor = config['bgcolor'].nil?      ? 'none'                    : config['bgcolor']
    scale = config['scale'].nil?          ? '100%'                        : config['scale']

    clength = clength.to_i
    size = (clength+1) * fontsize.to_i
    content = get_content(content, clength)

    #
    #@command << "-page +#{x}+#{y} \\\( -size #{size} -font '#{font}' -fill '#{fontcolor}' -pointsize #{fontsize} -kerning #{kerning} -background '#{bgcolor}' caption:\"#{content}\" \\\)"
    geometry = scale=='100%' ? "+#{x}+#{y}" : "#{scale}x#{scale}+#{x}+#{y}"
    @command << "\\\( -size #{size} -font '#{font}' -fill '#{fontcolor}' -pointsize #{fontsize} -kerning #{kerning} -background '#{bgcolor}' caption:\"#{content}\" \\\) -geometry #{geometry} -composite"
  end

  #在原图基础上先crop 后scale
  def add_image(x, y, image, config={})
    if image.start_with? 'http'
      image = download(image)
    else
      image = nil if not FileTest.exist?(image)
    end

    if not image.nil?
      scale = config['scale'].nil? ? '100%x100%' : config['scale']
      x = x.to_f
      y = y.to_f
      x = x < 0 ? "-#{x.abs}" : "+#{x.abs}"
      y = y < 0 ? "-#{y.abs}" : "+#{y.abs}"
      if scale.include?('%') and !scale.include?('x')
        scale = "#{scale}x#{scale}"
      end
      geometry = scale=='100%x100%' ? "#{x}#{y}" : "#{scale}#{x}#{y}"
      crop = config['crop'].nil? ? ' ' : "-crop #{config['crop'].gsub('p', '+')} "
      @command << "\\\( '#{image}' #{crop}\\\) -geometry #{geometry} -composite" 
    end

  end

  #顺时针
  def generate(file, rotate_degree=nil)
    if not rotate_degree.nil? 
      #@command << "-background none -layers flatten miff:- | convert -rotate #{rotate_degree} - '#{file}'"
      @command << "miff:- | convert -rotate #{rotate_degree} - '#{file}'"
    else
      #@command << "-background none -layers flatten '#{file}'"
      @command << "'#{file}'"
    end
    
    #puts @command.join(" \\\n")
    `#{@command.join(" ")}`
  end

  #不管怎么样，都生成jpg，imagemagick自动会进行图片格式转换
  def Stikker.generate(json, file, rotate_degree=nil)
    return nil if json.nil? or json.strip==''
    return nil if json.include? '<html>'

    #开始生成
    covers_objs = JSON.parse(json)

    #背景
    background = covers_objs['background'].nil? ? 'http://pic.52mxp.com/backgrounds/white_1500x1000.png' : covers_objs["background"]
    stikker = Stikker.new(background)

    #图片
    if(not covers_objs['images'].nil?)
      covers_objs['images'].each do |i|
        src = i['src']
        x = i['x']
        y = i['y']
        scale = i["scale"]
        crop = i["crop"]

        if not src.nil? and src.strip != ''
          stikker.add_image(x, y, src, {'scale'=>scale, 'crop'=>crop})
        end
      end
    end

    #文字
    if(not covers_objs['texts'].nil?)
      covers_objs['texts'].each do |t|
        text = t['content']
        x = t['x']
        y = t['y']
        size = t['size']
        color = t['color']
        kerning = t['kerning']
        # rotate = t['rotate']
        # width = t['width']
        scale = t["scale"]
        clength = t["clength"]

        if not text.nil? and text.strip != ''
          stikker.add_text(x, y, text, 'clength'=>clength, 'fontsize'=>size, 'fontcolor'=>color, 'kerning'=>kerning, 'scale'=>scale)
        end
      end
    end
    
    stikker.generate(file, rotate_degree)
  end

  private

  def download(url)

    if (suffix=url.split('.').last) == 'png' or suffix == 'jpg' or suffix == 'jpeg'
      filename = "#{Digest::SHA1.hexdigest(url)}.#{suffix}"
    else
      filename = "#{Digest::SHA1.hexdigest(url)}"
    end
    tmp_dir = "#{@dir}/tmp"
    FileUtils.mkdir(tmp_dir) unless File.directory?(tmp_dir)
    if FileTest.exist?("#{tmp_dir}/#{filename}")
      return "#{tmp_dir}/#{filename}"
    else
      puts "download #{url} ..."
      data = open(URI::encode(url)){|f| f.read} 
      file = File.new "#{tmp_dir}/#{filename}", 'w+' 
      file.binmode 
      file << data 
      file.flush 
      file.close
      return "#{tmp_dir}/#{filename}"
    end
    
  rescue 
    return nil
  end

end
